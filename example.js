const { default: paginate } = require('./lib');

const paginator = paginate(100, 32, { offset: 4 });

const { 
  displayNextElipsis, 
  displayPrevElipsis,
  isFirst,
  isLast,
  current,
  first,
  last,
  nextPages,
  prevPages,
  nextPage,
  prevPage,
} = paginator

const buttons = [
  ...(!isFirst ? ['first'] : []),
  ...(prevPage ? ['prev'] : []),
  ...(displayPrevElipsis ? ['...'] : []),
  ...prevPages,
  `[${current}]`,
  ...nextPages,
  ...(displayNextElipsis ? ['...'] : []),
  ...(nextPage ? ['next'] : []),
  ...(!isLast ? ['last'] : []), 
]

console.log(buttons.join(' '));