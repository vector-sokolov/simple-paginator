"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function paginate(total, current, options = {
    offset: 3,
}) {
    const pages = Array.from({ length: total }, (_, index) => index + 1);
    const { offset } = options;
    const first = pages[0];
    const last = pages[pages.length - 1];
    const isLast = current === total;
    const isFirst = current === first;
    const prevPagesCount = current > offset ? offset : current - 1;
    const prevPages = pages.slice(pages.indexOf(current) - prevPagesCount, pages.indexOf(current));
    const nextPagesCount = total - current > offset ? offset : total - current;
    const nextPages = pages.slice(pages.indexOf(current) + 1, pages.indexOf(current) + 1 + nextPagesCount);
    const nextPage = isLast ? null : pages[pages.indexOf(current) + 1];
    const prevPage = isFirst ? null : pages[pages.indexOf(current) - 1];
    const displayPrevElipsis = current - offset > 1;
    const displayNextElipsis = total - current > offset;
    return {
        total,
        current,
        first,
        last,
        pages,
        prevPages,
        nextPages,
        nextPage,
        prevPage,
        displayPrevElipsis,
        displayNextElipsis,
        isFirst,
        isLast,
    };
}
exports.default = paginate;
